import lombok.extern.slf4j.Slf4j;
import models.BotConfig;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import services.CurrencyRatesService;

import java.io.IOException;

@Slf4j
public class Application {

    public static void main(String[] args) throws IOException {

        log.info("Trying to init context");
        ApiContextInitializer.init();
        log.debug("Context was init");

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        log.info("Getting currency exchange service instance");
        CurrencyRatesService service = CurrencyRatesService.getInstance();
        log.debug("Service instance got");

        log.info("Trying to set bot config");
        BotConfig config = BotConfig.builder()
                .name(System.getenv("BOT_USERNAME"))
                .token(System.getenv("BOT_TOKEN"))
                .build();
        log.debug("Bot config: {}", config);

        Bot bot = new Bot(config, service);

        try {
            log.info("Trying to to register currency exchange bot");
            telegramBotsApi.registerBot(bot);
            log.info("Currency exchange bot was registered");
        } catch (TelegramApiRequestException e) {
            log.error("Currency exchange bot can't be registered");
            e.printStackTrace();
        }
    }
}
