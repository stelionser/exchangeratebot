package models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrencyRate {
    private Integer r030;
    private String txt;
    private String rate;
    private String cc;
}
