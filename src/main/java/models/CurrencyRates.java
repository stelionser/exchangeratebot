package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class CurrencyRates {
    private HashMap<String, CurrencyRate> items;
    private Date lastUpdated;
}
