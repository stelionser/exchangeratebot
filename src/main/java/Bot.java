import lombok.extern.slf4j.Slf4j;
import models.BotConfig;
import models.CurrencyRate;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import services.CurrencyRatesService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Bot extends TelegramLongPollingBot {

    private final BotConfig config;
    private CurrencyRatesService service;

    public Bot(BotConfig config, CurrencyRatesService service) {
        this.config = config;
        this.service = service;
    }

    public void onUpdateReceived(Update update) {
        log.info("Getting update request");
        log.debug("Getting message model");
        SendMessage messageModel = getMessageModel(update);

        try {
            log.debug("Try to execute bot api method SendMessage");
            log.trace("SendMessage model: {}", messageModel);
            execute(messageModel);
            log.debug("Method execution successful");
        } catch (TelegramApiException e) {
            log.error("Can't execute bot api method for reason: {}", e.getMessage());
            e.printStackTrace();
        }
    }

    private SendMessage getMessageModel(Update update) {
        log.debug("Trying to get currency rates data from currency service");
        CurrencyRate rate = service.getCurrencyRates().getItems().get(update.getMessage().getText());
        log.trace("Got information: {}", rate);

        SendMessage msg = new SendMessage();
        msg.enableMarkdown(true);
        msg.setChatId(update.getMessage().getChatId());
        msg.setText(rate == null
                ? "Can't get currency rate"
                : String.format("1 %s (%s) = %s UAH (Гривны)", rate.getCc(), rate.getTxt(), rate.getRate()));

        setButtons(msg);
        return msg;
    }

    public String getBotUsername() {
        return config.getName();
    }

    public String getBotToken() {
        return config.getToken();
    }

    private synchronized void setButtons(SendMessage sendMessage) {
        log.debug("Trying to set keyboard buttons with currency codes");
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        log.debug("Try to get currency codes from currency service and set they as KeyboardButtons");
        List<KeyboardButton> buttons = service.getCurrencyCodes().stream().map(KeyboardButton::new).collect(Collectors.toList());
        List<KeyboardRow> keyboardRows = new ArrayList<>();

        log.trace("Collect buttons in the keyboard with 5 button in a row");
        for (int b = 0, e = 5; b < buttons.size(); b += 5, e += 5) {
            List<KeyboardButton> keyboardButtons = buttons.subList(b, Math.min(e, buttons.size()));
            KeyboardRow row = new KeyboardRow();
            row.addAll(keyboardButtons);
            keyboardRows.add(row);
        }

        List<KeyboardRow> keyboard = new ArrayList<>(keyboardRows);
        replyKeyboardMarkup.setKeyboard(keyboard);
        log.debug("Keyboard setting has been completed ");
    }
}
