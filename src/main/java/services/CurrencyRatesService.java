package services;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import models.CurrencyRate;
import models.CurrencyRates;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Slf4j
public class CurrencyRatesService {

    private static CurrencyRatesService instance;

    private static final String URL_GET_ALL_RATES = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

    private Set<String> currencyCodes;
    private CurrencyRates currencyRates;

    // ======================================== PUBLIC METHODS =======================================================//

    public static CurrencyRatesService getInstance() {
        if (instance == null) {
            log.debug("Create CurrencyRatesService instance");
            instance = new CurrencyRatesService();
        } else {
            log.debug("Return exist CurrencyRatesService instance");
        }
        return instance;
    }

    public CurrencyRates getCurrencyRates() {
        log.debug("Getting currency rates");
        if (currencyRates == null
                || currencyRates.getItems().isEmpty()
                || (new Date().getTime() - currencyRates.getLastUpdated().getTime()) / 60 * 60 * 1000 > 1
        ) {
            log.trace("Current currency rates: {}", currencyRates);
            log.debug("Currency rates either not set or needs to be updated");
            currencyRates = mapToRates(parseCurrencyRates());
        }

        log.trace("Return currency rates: {}", currencyRates);
        return currencyRates;
    }

    public Set<String> getCurrencyCodes() {
        log.debug("Getting currency codes");
        if (isEmpty(currencyCodes)) {
            log.debug("Currency codes was empty. Try to get currency codes from new currency rates data");
            currencyCodes = mapToCurrencyCodes(isEmpty(currencyCodes)
                    ? parseCurrencyRates()
                    : (ArrayList<CurrencyRate>) currencyRates.getItems().values());
        }

        log.trace("Return currency codes: {}", currencyCodes);
        return currencyCodes;
    }

    // ======================================= PRIVATE METHODS =======================================================//

    private CurrencyRatesService() {
        log.debug("Trying to init CurrencyRatesService");
        init();
        log.debug("Service was init");
    }

    private void init() {
        getCurrencyCodes();
    }

    private List<CurrencyRate> parseCurrencyRates() {
        log.debug("Trying to get and parse currency rates from external service");
        try {
            log.debug("Trying to connect with external service: {}", URL_GET_ALL_RATES);
            URLConnection request = new URL(URL_GET_ALL_RATES).openConnection();
            request.connect();
            log.debug("Connection to the server has been established");

            log.debug("Trying to parse the received information ");
            JsonArray jsonArray = new JsonParser().parse(new InputStreamReader((InputStream) request.getContent())).getAsJsonArray();
            List<CurrencyRate> result = new Gson().fromJson(jsonArray, new TypeToken<List<CurrencyRate>>() {
            }.getType());

            log.trace("Parsed data model: {}", result);
            return result;

        } catch (IOException e) {
            log.error("Can't get and parse currency rates from external service because of the reason: {}", e.getMessage());
            log.debug("Return empty currency rates data model");
            return Collections.emptyList();
        }
    }

    // ========================================= MAPPERS =============================================================//

    private CurrencyRates mapToRates(List<CurrencyRate> rates) {
        log.debug("Trying to map list of currency rates to CurrencyRates model");
        log.trace("List of currency rates: {}", rates);
        Map<String, CurrencyRate> collect = rates
                .stream()
                .collect(Collectors.toMap(CurrencyRate::getCc, c -> c));

        CurrencyRates currencyRates = CurrencyRates.builder()
                .items(new HashMap<String, CurrencyRate>(collect))
                .lastUpdated(new Date())
                .build();

        log.trace("Mapped CurrencyRates model: {}", currencyRates);
        return currencyRates;
    }

    private Set<String> mapToCurrencyCodes(List<CurrencyRate> rates) {
        log.debug("Trying to map list of currency rates to currency codes");
        log.trace("List of currency rates: {}", rates);
        Set<String> result = rates
                .stream().map(CurrencyRate::getCc)
                .collect(Collectors.toSet());
        log.trace("Mapped currency codes: {}", result);
        return result;
    }
}
